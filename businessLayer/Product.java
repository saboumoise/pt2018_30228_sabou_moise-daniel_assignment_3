package businessLayer;

public class Product {
	private String name;
	private String producer;
	private int price;
	private int quantity;
	
	public Product(String name, String producer, int price, int quantity)
	{
		this.name = name;
		this.producer = producer;
		this.price = price;
		this.quantity = quantity;
	}
	
	public String getName()
	{
		return this.name;
	}
	public String getProducer()
	{
		return this.producer;
	}
	public int getPrice()
	{
		return this.price;
	}
	public int getQuantity()
	{
		return this.quantity;
	}
}
