package businessLayer;

public class Client {
	private String lastName;
	private String firstName;
	private String email;
	private int orders;
	
	public Client(String lastname, String firstname, String email, int orders)
	{
		this.lastName = lastname;
		this.firstName = firstname;
		this.email = email;
		this.orders = orders;
	}
	
	public String getLastname()
	{
		return this.lastName;
	}
	public String getFirsname()
	{
		return this.firstName;
	}
	public String getEmail()
	{
		return this.email;
	}
	public int getOrders()
	{
		return this.orders;
	}
}
