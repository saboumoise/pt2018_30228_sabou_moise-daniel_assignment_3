package businessLayer;

public class Order {
	private int clientId;
	private int ProductId;
	private int quantity;
	
	public Order(int cId, int pId, int qu)
	{
		this.setClientId(cId);
		this.setProductId(pId);
		this.setQuantity(qu);
	}
	
	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getProductId() {
		return ProductId;
	}

	public void setProductId(int productId) {
		ProductId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
