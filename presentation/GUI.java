package presentation;

import javax.swing.*;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import businessLayer.Client;
import businessLayer.Product;
import dataAccessLayer.connect;

public class GUI {
	public static void main(String args[]) throws Exception
	{
		JFrame frame = new JFrame("WareHouse");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1000, 1000);  

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////Clients
////////////////////////////////////////////////////////////////////////////////////////////	view all clients		
		JScrollPane allClientsPane = new JScrollPane();
		allClientsPane.setPreferredSize(new Dimension(300, 380));
		
		JButton clientsButton = new JButton(new AbstractAction("View all clients") 
		{
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) 
			{
				try 
				{
					JTable ClientsTable = new JTable(connect.getClients(), new String[] {"id", "lastname", "firstname", "email", "orders"});
					allClientsPane.getViewport().add(ClientsTable);
				}catch(Exception e) {System.out.print(e);}
			}
		});
		
		JPanel viewAllClientsPane = new JPanel();
		viewAllClientsPane.setPreferredSize(new Dimension(450, 390));
		viewAllClientsPane.setBorder(BorderFactory.createLineBorder(null));
		viewAllClientsPane.add(allClientsPane);
		viewAllClientsPane.add(clientsButton);
////////////////////////////////////////////////////////////////////////////////////////////     edit clients

//////////////////////////////////////////////////////////////////////add client
		JPanel addClientPane = new JPanel();
		addClientPane.setPreferredSize(new Dimension(450, 130));
		addClientPane.setBorder(BorderFactory.createLineBorder(null));
		
		JTextField lastname = new JTextField("last name      ");
		JTextField firstname = new JTextField("first name     ");
		JTextField email = new JTextField("email          ");
		JTextField orders = new JTextField("orders         ");
		JButton addClient = new JButton(new AbstractAction("Add Client")
		{
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0)
			{
				try
				{
					Client newClient = new Client(lastname.getText(), firstname.getText(), email.getText(), Integer.parseInt(orders.getText()));
					connect.addClient(newClient);
				}catch(Exception e) {System.out.println(e);}
			}
		});
		addClientPane.add(lastname);
		addClientPane.add(firstname);
		addClientPane.add(email);
		addClientPane.add(orders);
		addClientPane.add(addClient);
		
////////////////////////////////////////////////////////////////////edit client
		
		JPanel editClientPane = new JPanel();
		editClientPane.setPreferredSize(new Dimension(450, 130));
		editClientPane.setBorder(BorderFactory.createLineBorder(null));
		
		JTextField idE = new JTextField("id            ");
		JTextField lastnameE = new JTextField("last name      ");
		JTextField firstnameE = new JTextField("first name     ");
		JTextField emailE = new JTextField("email          ");
		JTextField ordersE = new JTextField("orders         ");
		JButton editClient = new JButton(new AbstractAction("Edit Client")
		{
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0)
			{
				try
				{
					Client newValues = new Client(lastnameE.getText(), firstnameE.getText(), emailE.getText(), Integer.parseInt(ordersE.getText()));
					connect.editClient(Integer.parseInt(idE.getText()), newValues);
				}catch(Exception e) {System.out.println(e);}
			}
		});
		editClientPane.add(idE);
		editClientPane.add(lastnameE);
		editClientPane.add(firstnameE);
		editClientPane.add(emailE);
		editClientPane.add(ordersE);
		editClientPane.add(editClient);
		
////////////////////////////////////////////////////////////////////delete client
		
		JPanel deleteClientPane = new JPanel();
		deleteClientPane.setPreferredSize(new Dimension(450, 107));
		deleteClientPane.setBorder(BorderFactory.createLineBorder(null));
		
		JTextField idD = new JTextField("id             ");
		JButton deleteClient = new JButton(new AbstractAction("Delete Client")
		{
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0)
			{
				try
				{
					connect.deleteClient(Integer.parseInt(idD.getText()));
				}catch(Exception e) {System.out.println(e);}
			}
		});
		
		deleteClientPane.add(idD);
		deleteClientPane.add(deleteClient);
		
		
////////////////////////////////////////////////////////////////////adds
		
		JPanel editClientsPane = new JPanel();
		editClientsPane.setPreferredSize(new Dimension(450, 390));
		editClientsPane.setBorder(BorderFactory.createLineBorder(null));
		editClientsPane.add(addClientPane);
		editClientsPane.add(editClientPane);
		editClientsPane.add(deleteClientPane);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////Products		
////////////////////////////////////////////////////////////////////////////////////////////view all Products		
		JScrollPane allProductsPane = new JScrollPane();
		allProductsPane.setPreferredSize(new Dimension(300, 380));
		
		JButton ProductsButton = new JButton(new AbstractAction("View all Products") 
		{
			private static final long serialVersionUID = 1L;
	
			public void actionPerformed(ActionEvent arg0) 
			{
				try 
				{
					JTable ProductsTable = new JTable(connect.getProducts(), new String[] {"id", "name", "producer", "price", "quantity"});
					allProductsPane.getViewport().add(ProductsTable);
				}catch(Exception e) {System.out.println(e);}
			}
		});
		
		JPanel viewAllProductsPane = new JPanel();
		viewAllProductsPane.setPreferredSize(new Dimension(450, 390));
		viewAllProductsPane.setBorder(BorderFactory.createLineBorder(null));
		viewAllProductsPane.add(allProductsPane);
		viewAllProductsPane.add(ProductsButton);
	
////////////////////////////////////////////////////////////////////////////////////////////edit Products
	
//////////////////////////////////////////////////////////////////////add Product
		JPanel addProductPane = new JPanel();
		addProductPane.setPreferredSize(new Dimension(450, 130));
		addProductPane.setBorder(BorderFactory.createLineBorder(null));
		
		JTextField name = new JTextField("name           ");
		JTextField producer = new JTextField("producer       ");
		JTextField price = new JTextField("price          ");
		JTextField quantity = new JTextField("quantity      ");
		JButton addProduct = new JButton(new AbstractAction("Add Product")
		{
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0)
			{
				try
				{
					Product newProduct = new Product(name.getText(), producer.getText(), 
							Integer.parseInt(price.getText()), Integer.parseInt(quantity.getText()));
					connect.addProduct(newProduct);
				}catch(Exception e) {System.out.println(e);}
			}
		});
		addProductPane.add(name);
		addProductPane.add(producer);
		addProductPane.add(price);
		addProductPane.add(quantity);
		addProductPane.add(addProduct);
	
////////////////////////////////////////////////////////////////////edit Product
	
		JPanel editProductPane = new JPanel();
		editProductPane.setPreferredSize(new Dimension(450, 130));
		editProductPane.setBorder(BorderFactory.createLineBorder(null));
		
		JTextField productIdE = new JTextField("id            ");
		JTextField nameE = new JTextField("name           ");
		JTextField producerE = new JTextField("producer       ");
		JTextField priceE = new JTextField("price          ");
		JTextField quantityE = new JTextField("quantity       ");
		JButton editProduct = new JButton(new AbstractAction("Edit Product")
		{
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0)
			{
				try
				{
					Product newValues = new Product(nameE.getText(), producerE.getText(), 
							Integer.parseInt(priceE.getText()), Integer.parseInt(quantityE.getText()));
					connect.editProduct(Integer.parseInt(productIdE.getText()), newValues);
				}catch(Exception e) {System.out.println(e);}
			}
		});
		editProductPane.add(productIdE);
		editProductPane.add(nameE);
		editProductPane.add(producerE);
		editProductPane.add(priceE);
		editProductPane.add(quantityE);
		editProductPane.add(editProduct);
	
	////////////////////////////////////////////////////////////////////delete Product
	
		JPanel deleteProductPane = new JPanel();
		deleteProductPane.setPreferredSize(new Dimension(450, 107));
		deleteProductPane.setBorder(BorderFactory.createLineBorder(null));
		
		JTextField productIdD = new JTextField("id             ");
		JButton deleteProduct = new JButton(new AbstractAction("Delete Product")
		{
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0)
			{
				try
				{
					connect.deleteProduct(Integer.parseInt(productIdD.getText()));
				}catch(Exception e) {System.out.println(e);}
			}
		});
		
		deleteProductPane.add(productIdD);
		deleteProductPane.add(deleteProduct);
	
	
	////////////////////////////////////////////////////////////////////adds
	

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////Order
		
		JTextField clientId = new JTextField("Client ID      ");
		JTextField productId = new JTextField("Product ID      ");
		JTextField quantityO = new JTextField("Quantity       ");
		JButton order = new JButton(new AbstractAction("Order")
		{
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0)
			{
				try
				{
					connect.order(Integer.parseInt(clientId.getText()), Integer.parseInt(productId.getText()), Integer.parseInt(quantityO.getText()));
				}catch(Exception e) {System.out.println(e);}
			}
		});
		
		JPanel orderMainPane = new JPanel();
		orderMainPane.setPreferredSize(new Dimension(1000, 180));
		orderMainPane.setBorder(BorderFactory.createLineBorder(null));
		orderMainPane.add(clientId);
		orderMainPane.add(productId);
		orderMainPane.add(quantityO);
		orderMainPane.add(order);
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		JPanel editProductsPane = new JPanel();
		editProductsPane.setPreferredSize(new Dimension(450, 390));
		editProductsPane.setBorder(BorderFactory.createLineBorder(null));
		editProductsPane.add(addProductPane);
		editProductsPane.add(editProductPane);
		editProductsPane.add(deleteProductPane);		

		
		JPanel productsMainPane = new JPanel();
		productsMainPane.setPreferredSize(new Dimension(1000, 400));
		productsMainPane.setBorder(BorderFactory.createLineBorder(null));
		productsMainPane.add(viewAllProductsPane);
		productsMainPane.add(editProductsPane);
	
		JPanel clientsMainPane = new JPanel();
		clientsMainPane.setPreferredSize(new Dimension(1000, 400));
		clientsMainPane.setBorder(BorderFactory.createLineBorder(null));
		clientsMainPane.add(viewAllClientsPane);
		clientsMainPane.add(editClientsPane);
		
		JPanel MainPane = new JPanel();
		MainPane.add(clientsMainPane);
		MainPane.add(productsMainPane);
		MainPane.add(orderMainPane);
		
		frame.add(MainPane);
		frame.setVisible(true);
	}
}
