package dataAccessLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JOptionPane;

import businessLayer.Client;
import businessLayer.Product;

public class connect {
	public static Connection getConnection() throws Exception{
		try {
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost:3306/schooldb";
			String username = "root";
			String password = "Moise123";
			Class.forName(driver);
			
			Connection conn = DriverManager.getConnection(url, username, password);
			return conn;
		}catch(Exception e) {System.out.println(e);}
		
		return null;
	}
	
	public static String[][] getClients() throws Exception
	{
		try 
		{
			Connection con = getConnection();
			PreparedStatement selectClients = con.prepareStatement("SELECT id, lastname, firstname, email, orders FROM clients");
			
			ResultSet result = selectClients.executeQuery();
			String[][] clients = new String[100][5];
			int i = 0;
			while(result.next()) 
			{
				clients[i][0] = result.getString("id");
				clients[i][1] = result.getString("lastname");
				clients[i][2] = result.getString("firstname");
				clients[i][3] = result.getString("email");
				clients[i][4] = result.getString("orders");
				i++;
			}
			return clients;
		}catch(Exception e) {System.out.print(e);}
		
		return null;
	}
	
	public static String[][] getProducts() throws Exception
	{
		try 
		{
			Connection con = getConnection();
			PreparedStatement selectProducts = con.prepareStatement("SELECT id, name, producer, price, quantity FROM products");
			
			ResultSet result = selectProducts.executeQuery();
			String[][] products = new String[100][5];
			int i = 0;
			while(result.next()) 
			{
				products[i][0] = result.getString("id");
				products[i][1] = result.getString("name");
				products[i][2] = result.getString("producer");
				products[i][3] = result.getString("price");
				products[i][4] = result.getString("quantity");
				i++;
			}
			return products;
		}catch(Exception e) {System.out.print(e);}
		
		return null;
	}
	
	public static void addClient(Client newClient) throws Exception
	{
		try
		{
			Connection con = getConnection();
			PreparedStatement addClient = con.prepareStatement("INSERT INTO clients (firstname, lastname, email, orders) VALUES ('"+newClient.getFirsname()+
					"', '"+newClient.getLastname()+"', '"+newClient.getEmail()+"', "+newClient.getOrders()+")");
			addClient.executeUpdate();
			
		}catch(Exception e) {System.out.println(e);}
	}
	
	public static void addProduct(Product newProduct) throws Exception
	{
		try
		{
			Connection con = getConnection();
			PreparedStatement addProduct = con.prepareStatement("INSERT INTO products (name, producer, price, quantity) VALUES ('"+newProduct.getName()+
					"', '"+newProduct.getProducer()+"', "+newProduct.getPrice()+", "+newProduct.getQuantity()+")");
			addProduct.executeUpdate();
			
		}catch(Exception e) {System.out.println(e);}
	}
	
	public static void editClient(int id, Client newValues) throws Exception
	{
		try
		{
			Connection con = getConnection();
			PreparedStatement editClient = con.prepareStatement("UPDATE clients SET firstname = '"+newValues.getFirsname()+"', lastname = '"+
					newValues.getLastname()+"', email = '"+newValues.getEmail()+"', orders = "+newValues.getOrders()+" WHERE id = "+id+";");			
			editClient.executeUpdate();
			
		}catch(Exception e) {System.out.println(e);}
	}
	
	public static void deleteClient(int id) throws Exception
	{
		try
		{
			Connection con = getConnection();
			PreparedStatement editClient = con.prepareStatement("DELETE FROM clients WHERE id = "+id+";");			
			editClient.executeUpdate();
			
		}catch(Exception e) {System.out.println(e);}
	}
	
	public static void editProduct(int id, Product newValues) throws Exception
	{
		try
		{
			Connection con = getConnection();
			PreparedStatement editProduct = con.prepareStatement("UPDATE products SET name = '"+newValues.getName()+"', producer = '"+
					newValues.getProducer()+"', price = '"+newValues.getPrice()+"', quantity = "+newValues.getQuantity()+" WHERE id = "+id+";");			
			editProduct.executeUpdate();
			
		}catch(Exception e) {System.out.println(e);}
	}
	
	public static void deleteProduct(int id) throws Exception
	{
		try
		{
			Connection con = getConnection();
			PreparedStatement editProduct = con.prepareStatement("DELETE FROM products WHERE id = "+id+";");			
			editProduct.executeUpdate();
			
		}catch(Exception e) {System.out.println(e);}
	}
	
	public static void order(int cId, int pId, int qu)
	{
		try
		{
			Connection con = getConnection();
			PreparedStatement insertOrder = con.prepareStatement("INSERT INTO orders (clientid, productid, quantity) values ('"+cId+"', '"
					+pId+"', "+qu+")");
			PreparedStatement incClientOrders = con.prepareStatement("UPDATE clients SET orders = orders + 1 WHERE id = "+cId);
			PreparedStatement decProductQuantity = con.prepareStatement("UPDATE products SET quantity = quantity - "+qu+" WHERE id = "+pId);
			PreparedStatement findQuantity = con.prepareStatement("SELECT * FROM products WHERE id = "+pId);
			
			ResultSet result = findQuantity.executeQuery();
			result.next();
			
			if(Integer.parseInt(result.getString("quantity")) > qu)
			{
				insertOrder.executeUpdate();
				incClientOrders.executeUpdate();
				decProductQuantity.executeUpdate();
			}
			else
			{
				JOptionPane.showMessageDialog(null, "Insufficient Stock", "MESSAGE", JOptionPane.INFORMATION_MESSAGE);
			}
			
		}catch(Exception e) {System.out.println(e);}
	}
}
